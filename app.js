var markOne = "x", markTwo = "o"
var webView = new WebView()
var gameStatus = new GameStatus(markOne, markTwo)
var playerOne = new HumanPlayer(markOne)
var playerTwo = new ComputerPlayer(markTwo, new Intelligence(gameStatus))
var board = ["-", "-", "-", "-", "-","-", "-", "-", "-"]
var game = new GameState(webView, playerOne, playerTwo, gameStatus, board)
