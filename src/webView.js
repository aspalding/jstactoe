var WebView = function() { }

WebView.prototype.turn = function(mark, move, fn) {
  var element = document.getElementById("move" + move)
  element.style.color = "#000"
  element.style.cursor = "default"
  mark == "x" ? element.className = "fa fa-close fa-5x" : element.className = "fa fa-circle-o fa-5x"
  this.removeListener(fn, element)
}

WebView.prototype.tieStatus = function() {
  var element = document.getElementById("status")
  element.style.color = "#000"
  element.className = "fa fa-meh-o fa-3x"
  element.style.cursor = "default"
}

WebView.prototype.loseStatus = function() {
  var element = document.getElementById("status")
  element.style.color = "#000"
  element.className = "fa fa-frown-o fa-3x"
  element.style.cursor = "default"
}

WebView.prototype.attachListeners = function(fn, size) {
  for(var i = 0; i < size; i++){
    var element = document.getElementById("move" + i)
    element.addEventListener("click", fn)
  }
}

WebView.prototype.removeListener = function(fn, element) {
  element.removeEventListener("click", fn)
  element.style.cursor = "default"
}

WebView.prototype.removeListeners = function(fn, size) {
  for(var i = 0; i < size; i++){
    var element = document.getElementById("move" + i)
    this.removeListener(fn, element)
  }
}
