var MockView = function() { }

MockView.prototype.turn = function () { }

MockView.prototype.tieStatus = function () { }

MockView.prototype.loseStatus = function () { }

MockView.prototype.attachListeners = function () { }

MockView.prototype.removeListeners = function () { }


var MockPlayer = function(mark) { this.mark = mark }

MockPlayer.prototype.getMove = function(board, args) { return 0; }

var MockIntelligence = function() { }

MockIntelligence.prototype.smartMove = function(mark, board) { return 0; }


var MockStatus = function(tieGame, winnerGame) { this.tieGame = tieGame; this.winnerGame = winnerGame }

MockStatus.prototype.tie = function (board) { return this.tieGame }

MockStatus.prototype.winner = function (mark, board) { return this.winnerGame }
