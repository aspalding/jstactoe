var Util = Util || {}

Util.maxInt = function(object) {
  var maxKey = -1, maxValue = -100
  for(var key in object)
    if(object[key] > maxValue)
      maxValue = object[key], maxKey = key
  return maxKey
}

Util.defined = function(element) {
  return element != undefined
}
