var GameState = function(view, playerOne, playerTwo, gameStatus, board) {
  this.view = view
  this.gameStatus = gameStatus
  this.playerOne = playerOne
  this.playerTwo = playerTwo
  this.board = board
  this.listenFn = this.turn.bind(this)
  this.view.attachListeners(this.listenFn, this.board.length)
}

GameState.prototype.turn = function(userEvent) {
  this.playMove(this.playerOne, userEvent)
  if(this.endGame()) return;
  this.playMove(this.playerTwo, userEvent)
}

GameState.prototype.playMove = function(player, userEvent) {
  var move = player.getMove(this.board, userEvent)
  this.placeMove(move, player.mark)
  this.endGame()
}

GameState.prototype.placeMove = function(move, mark) {
  this.board[move] = mark
  this.view.turn(mark, move, this.listenFn)
}

GameState.prototype.endGame = function() {
  if(this.gameStatus.tie(this.board)) {
    this.view.tieStatus()
    return true
  }
  if(this.gameStatus.winner(this.playerTwo.mark, this.board)) {
    this.view.removeListeners(this.listenFn, this.board.length)
    this.view.loseStatus()
    return true
  }
  else
    return false
}
