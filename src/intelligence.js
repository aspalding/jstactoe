var Intelligence = function(gameStatus) {
  this.gameStatus = gameStatus
}

Intelligence.prototype.score = function(board, depth) {
  return this.gameStatus.won(board) ? -10 + depth : 0
}

Intelligence.prototype.scoreMove = function(mark, board, depth, scores) {
  if(this.gameStatus.terminal(board)) return this.score(board, depth)

  var ref = this
  this.gameStatus.emptyIndices(board).forEach(function (position) {
    board[position] = mark
    scores[position] = -1 * ref.scoreMove(ref.gameStatus.opposingMark(mark), board, depth + 1, {})
    board[position] = "-"
  })

  var bestMove = Util.maxInt(scores); bestScore = scores[bestMove]
  return depth === 0 ? bestMove : bestScore
}

Intelligence.prototype.smartMove = function(mark, board) {
  return this.scoreMove(mark, board, 0, {})
}
