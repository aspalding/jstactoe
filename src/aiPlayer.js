var ComputerPlayer = function(mark, intelligence) {
  this.mark = mark
  this.intelligence = intelligence
}

ComputerPlayer.prototype.getMove = function(board, args) {
  return this.intelligence.smartMove(this.mark, board)
}
