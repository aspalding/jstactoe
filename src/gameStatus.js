var GameStatus = function(playerOneMark, playerTwoMark) {
  this.playerOneMark = playerOneMark
  this.playerTwoMark = playerTwoMark
}

GameStatus.prototype.opposingMark = function(mark) {
  return mark === this.playerOneMark ? this.playerTwoMark : this.playerOneMark
}

GameStatus.prototype.won = function(board) {
  return this.winner(this.playerTwoMark, board) || this.winner(this.playerOneMark, board)
}

GameStatus.prototype.winner = function(mark, board) {
  return this.horizontal(mark, board) || this.vertical(mark, board) || this.diagonal(mark, board)
}

GameStatus.prototype.full = function(board) {
  return !board.some(function(position) { return position === "-" } )
}

GameStatus.prototype.terminal = function(board) {
  return this.winner(this.playerTwoMark, board) || this.winner(this.playerOneMark, board) || this.full(board)
}

GameStatus.prototype.tie = function(board) {
  return this.full(board) && !this.winner(this.playerOneMark, board) && !this.winner(this.playerTwoMark, board)
}

GameStatus.prototype.emptyIndex = function(element, index) {
  if(element == "-") return index
}

GameStatus.prototype.emptyIndices = function(board) {
  return board.map(this.emptyIndex).filter(Util.defined)
}

GameStatus.prototype.horizontal = function(mark, board) {
  return (board[0] === mark && board[1] === mark && board[2] === mark) ||
         (board[3] === mark && board[4] === mark && board[5] === mark) ||
         (board[6] === mark && board[7] === mark && board[8] === mark)
}
GameStatus.prototype.vertical = function(mark, board) {
  return (board[0] === mark && board[3] === mark && board[6] === mark) ||
         (board[1] === mark && board[4] === mark && board[7] === mark) ||
         (board[2] === mark && board[5] === mark && board[8] === mark)
}

GameStatus.prototype.diagonal = function(mark, board) {
  return (board[0] === mark && board[4] === mark && board[8] === mark) ||
         (board[2] === mark && board[4] === mark && board[6] === mark)
}
