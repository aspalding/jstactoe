describe("A computer player", function() {
  var intelligence = new MockIntelligence()
  var player = new ComputerPlayer("o", intelligence)

  it("takes it's mark and intelligence as a parameter", function() {
    expect(player.mark).toEqual("o")
    expect(player.intelligence).toEqual(intelligence)
  })

  it("returns a number", function() {
    board = []; args = ""
    expect(player.getMove(board, args)).toEqual(0)
  })

  it("calls smartMove", function() {
    spyOn(intelligence, "smartMove")
    board = []; args = ""
    player.getMove(board, args)
    expect(intelligence.smartMove).toHaveBeenCalled()
  })
})
