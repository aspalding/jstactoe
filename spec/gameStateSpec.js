describe("The state of the board", function() {
  var view = new MockView()
  var playerOne = new MockPlayer("x")
  var playerTwo = new MockPlayer("o")
  var gameStatus = new MockStatus(false, false)
  var board = ["-", "-", "-", "-", "-","-", "-", "-", "-"]
  var state = new GameState(view, playerOne, playerTwo, gameStatus, board)

  it("takes type of view, players, and game status as constructor arguments", function() {
    expect(state.view).toEqual(view)
    expect(state.gameStatus).toEqual(gameStatus)
    expect(state.playerOne).toEqual(playerOne)
    expect(state.playerTwo).toEqual(playerTwo)
    expect(state.board).toEqual(board)
  })

  describe("turn", function() {
    it("calls play turn once for each player", function() {
      spyOn(state, "playMove")
      state.turn("")
      expect(state.playMove.calls.count()).toEqual(2)
    })

    it("calls end game to stop the turn in the case of a tie", function() {
      spyOn(state, "endGame")
      state.turn("")
      expect(state.endGame).toHaveBeenCalled()
    })
  })

  describe("playMove", function() {
    it("gets move from player", function() {
      spyOn(playerOne, "getMove")
      state.playMove(playerOne, "")
      expect(playerOne.getMove).toHaveBeenCalled()
    })

    it("places the move and checks the status of the game", function() {
      spyOn(state, "placeMove")
      spyOn(state, "endGame")
      state.playMove(playerOne, "")
      expect(state.endGame).toHaveBeenCalled()
      expect(state.placeMove).toHaveBeenCalled()
    })
  })

  describe("placeMove", function() {
    it("tells view to render a move", function() {
      spyOn(view, "turn")
      state.placeMove(0, "x")
      expect(view.turn).toHaveBeenCalled()
    })

    it("places a mark", function() {
      state.placeMove(0, "x")
      expect(state.board[0]).toEqual("x")
    })
  })

  describe("endGame", function() {
    var tieStatus = new MockStatus(true, false)
    var stateTie = new GameState(view, playerOne, playerTwo, tieStatus, board)

    var winStatus = new MockStatus(false, true)
    var stateWinner = new GameState(view, playerOne, playerTwo, winStatus, board)

    it("reports a tie", function() {
      spyOn(view, "tieStatus")
      expect(stateTie.endGame()).toEqual(true)
      expect(view.tieStatus).toHaveBeenCalled()
    })

    it("reports a lose and removes extra listeners", function() {
      spyOn(view, "loseStatus")
      spyOn(view, "removeListeners")
      expect(stateWinner.endGame()).toEqual(true)
      expect(view.loseStatus).toHaveBeenCalled()
      expect(view.removeListeners).toHaveBeenCalled()
    })

    it("returns false if the game is not terminal", function() {
      expect(state.endGame()).toEqual(false)
    })
  })
})
