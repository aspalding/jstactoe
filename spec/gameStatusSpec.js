describe("The status of the board", function() {
  var gameStatus = new GameStatus("x", "o")

  it("takes the player marks as arguments", function() {
    expect(gameStatus.playerOneMark).toEqual("x")
    expect(gameStatus.playerTwoMark).toEqual("o")
  })

  it("can determine the opposing player", function() {
    expect(gameStatus.opposingMark("x")).toEqual("o")
    expect(gameStatus.opposingMark("o")).toEqual("x")
  })

  it("winner should be true", function() {
    var board0 = ["x", "x", "x", 
                  "-", "-", "-", 
                  "-", "-", "-"] 

    var board1 = ["x", "-", "-", 
                  "x", "-", "-", 
                  "x", "-", "-"] 

    var board2 = ["-", "-", "x", 
                  "-", "x", "-", 
                  "x", "-", "-"] 

    expect(gameStatus.winner("x", board0)).toBe(true) 
    expect(gameStatus.winner("x", board1)).toBe(true) 
    expect(gameStatus.winner("x", board2)).toBe(true) 
  }) 


  it("winner should be false", function() {
    var board0 = ["x", "o", "x", 
                  "-", "-", "-", 
                  "-", "-", "-"] 

    var board1 = ["x", "-", "-", 
                  "x", "-", "-", 
                  "o", "-", "-"] 

    var board2 = ["-", "-", "o", 
                  "-", "x", "-", 
                  "x", "-", "-"] 

    expect(gameStatus.winner("x", board0)).toBe(false) 
    expect(gameStatus.winner("x", board1)).toBe(false) 
    expect(gameStatus.winner("x", board2)).toBe(false) 
  }) 

  it("won should be true", function() {
    var board0 = ["x", "x", "x",
                  "-", "-", "-",
                  "-", "-", "-"]

    var board1 = ["o", "-", "-",
                  "o", "-", "-",
                  "o", "-", "-"]

    expect(gameStatus.won(board0)).toBe(true)
    expect(gameStatus.won(board1)).toBe(true)
  })

  it("won should be false", function() {
    var board0 = ["x", "o", "x",
                  "-", "-", "-",
                  "-", "-", "-"]

    var board1 = ["x", "-", "-",
                  "x", "-", "-",
                  "o", "-", "-"]

    expect(gameStatus.won(board0)).toBe(false)
    expect(gameStatus.won(board1)).toBe(false)
  })

  it("can determine a full board", function() {
    var board0 = ["x", "x", "x",
                  "x", "x", "x",
                  "x", "x", "x"]

    var board1 = ["x", "x", "x",
                  "x", "-", "x",
                  "x", "x", "x"]

    var board2 = ["o", "x", "x",
                  "x", "x", "x",
                  "x", "x", "x"]

    expect(gameStatus.full(board0)).toBe(true)
    expect(gameStatus.full(board1)).toBe(false)
    expect(gameStatus.full(board2)).toBe(true)
  })

  describe("terminal game", function() {
    it("false when board is not full and no winner has been decided", function() {
      var board = ["x", "x", "o", 
                   "-", "-", "-", 
                   "-", "-", "-"] 

      expect(gameStatus.terminal(board)).toBe(false) 
    }) 

    it("if there is a winner", function() {
      var board0 = ["x", "x", "x", 
                    "-", "-", "-", 
                    "-", "-", "-"] 

      var board1 = ["o", "o", "o", 
                    "-", "-", "-", 
                    "-", "-", "-"] 

      expect(gameStatus.terminal(board0)).toBe(true) 
      expect(gameStatus.terminal(board1)).toBe(true) 
    }) 

    it("if there is a full board", function() {
      var board = ["x", "o", "x", 
                   "x", "o", "x",
                   "o", "x", "o"] 

      expect(gameStatus.terminal(board)).toBe(true) 
    }) 

  }) 

  describe("horizontal win", function() {
    it("three in a row signifies a winner", function() {
      var board0 = ["x", "x", "x", 
                    "-", "-", "-", 
                    "-", "-", "-"] 

      var board1 = ["-", "-", "-", 
                    "x", "x", "x",
                    "-", "-", "-"] 

      var board2 = ["-", "-", "-", 
                    "-", "-", "-",
                    "x", "x", "x"] 

      expect(gameStatus.horizontal("x", board0)).toBe(true) 
      expect(gameStatus.horizontal("x", board1)).toBe(true) 
      expect(gameStatus.horizontal("x", board2)).toBe(true) 
    }) 

    it("two in a row does not signify a winner", function() {
      var board = ["x", "x", "-", 
                   "-", "-", "-", 
                   "-", "-", "-"] 

      expect(gameStatus.horizontal("x", board)).toBe(false) 
    }) 
  }) 

  describe("vertical win", function() {
    it("three in a row signifies a winner", function() {
      var board0 = ["x", "-", "-", 
                    "x", "-", "-", 
                    "x", "-", "-"] 

      var board1 = ["-", "x", "-", 
                    "-", "x", "-",
                    "-", "x", "-"] 

      var board2 = ["-", "-", "x", 
                    "-", "-", "x",
                    "-", "-", "x"] 

      expect(gameStatus.vertical("x", board0)).toBe(true) 
      expect(gameStatus.vertical("x", board1)).toBe(true) 
      expect(gameStatus.vertical("x", board2)).toBe(true) 
    }) 

    it("two in a row does not signify a winner", function() {
      var board = ["x", "-", "-", 
                   "x", "-", "-", 
                   "-", "-", "-"] 

      expect(gameStatus.vertical("x", board)).toBe(false) 
    }) 
  }) 

  describe("diagonal win", function() {
    it("three in a row signifies a winner", function() {
      var board0 = ["-", "-", "x", 
                    "-", "x", "-", 
                    "x", "-", "-"] 

      var board1 = ["x", "-", "-", 
                    "-", "x", "-",
                    "-", "-", "x"] 

      expect(gameStatus.diagonal("x", board0)).toBe(true) 
      expect(gameStatus.diagonal("x", board1)).toBe(true) 
    }) 

    it("two in a row does not signify a winner", function() {
      var board = ["x", "-", "-", 
                   "-", "x", "-", 
                   "-", "-", "-"] 

      expect(gameStatus.diagonal("x", board)).toBe(false) 
    }) 
  }) 

  describe("emptySpaces", function() {
    it("returns all spaces if board is empty", function() {
      var board = ["-", "-", "-", 
                   "-", "-", "-",
                   "-", "-", "-"]

      expect(gameStatus.emptyIndices(board)).toEqual([0, 1, 2, 3, 4, 5, 6, 7, 8])
    })

    it("returns empty spaces", function() {
      var board0 = ["-", "-", "x", 
                    "x", "x", "x",
                    "x", "x", "x"]

      var board1 = ["x", "x", "x", 
                    "x", "x", "x",
                    "x", "-", "-"]

      expect(gameStatus.emptyIndices(board0)).toEqual([0, 1])
      expect(gameStatus.emptyIndices(board1)).toEqual([7, 8])
    })
  })

  describe("tie", function() {
    it("reports a tie if there is no winner", function() {
      var board = ["x", "x", "o", 
                   "o", "o", "x",
                   "x", "x", "o"]

      expect(gameStatus.tie(board)).toBe(true)
    })

    it("doesn't reporting a winning board a tie", function() {
      var board = ["o", "x", "x", 
                   "x", "o", "x",
                   "o", "o", "x"]

      expect(gameStatus.tie(board)).toEqual(false)
    })
  })
}) 
