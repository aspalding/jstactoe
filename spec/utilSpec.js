describe("Javascript work arounds", function() {
  it("determines if an item is undefined", function() {
    expect(Util.defined(1)).toEqual(true)
    expect(Util.defined("")).toEqual(true)
    expect(Util.defined(undefined)).toEqual(false)
  }) 

  describe("maxInt function on a javascript object", function() {
    it("finds the index of the max element in a object", function() {
      var object = {"0": 100, "1": 10, "2": 1000}
      expect(Util.maxInt(object)).toEqual("2")
    })

    it("works for negative numbers", function() {
      var object = {"0": -100, "1": -10, "2": -1000}
      expect(Util.maxInt(object)).toEqual("1")
    })
  })
})
