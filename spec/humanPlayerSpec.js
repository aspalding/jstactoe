describe("A human player", function() {
  var player = new HumanPlayer("x")

  it("takes it's mark as a parameter", function() {
    expect(player.mark).toEqual("x")
  })

  it("returns a move from an event", function() {
    var args = {}; args.target = {"id": "move0"}; board = []
    expect(player.getMove(board, args)).toEqual("0")
  })
})
