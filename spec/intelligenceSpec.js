describe("The artificial intelligence", function() {
  var gameStatus = new GameStatus("x", "o")
  var ai = new Intelligence(gameStatus)

  it("takes game status as a parameter", function() {
    expect(ai.gameStatus).toEqual(gameStatus)
  })

  describe("negamax", function() {
    it("scores 0 if the game is undecided", function() {
      var board0 = ["-", "-", "-", 
                    "-", "-", "-",
                    "-", "o", "o"]

      var board1 = ["x", "x", "-", 
                    "-", "-", "-",
                    "-", "-", "-"]

      expect(ai.score(board0)).toEqual(0)
      expect(ai.score(board1)).toEqual(0)
    })

    it("scores -10 if there is a winner", function() {
      var board0 = ["-", "-", "-", 
                    "-", "-", "-",
                    "o", "o", "o"]

      var board1 = ["x", "x", "x", 
                    "-", "-", "-",
                    "-", "-", "-"]

      expect(ai.score(board0, 0)).toEqual(-10)
      expect(ai.score(board1, 0)).toEqual(-10)
    }) 

    it("picks necessary  move", function() {
      var board0 = ["x", "-", "-", 
                    "-", "x", "-",
                    "o", "-", "o"]

      var board1 = ["x", "-", "-", 
                    "o", "-", "-",
                    "-", "-", "x"]

      expect(ai.smartMove("x", board0)).toEqual("7") 
      expect(ai.smartMove("o", board1)).toEqual("4") 
    })

    it("blocks a fork", function() {
      var board0 = ["-", "-", "x", 
                    "-", "x", "-",
                    "o", "-", "-"]

      var board1 = ["x", "o", "x", 
                    "-", "-", "-",
                    "-", "-", "-"]

      expect(ai.smartMove("o", board0)).toEqual("0") 
      expect(ai.smartMove("o", board1)).toEqual("4")
    })

    it("takes a win immediately", function() {
      var board = ["-", "x", "x", 
                   "-", "x", "-",
                   "o", "o", "-"]

      expect(ai.smartMove("o", board)).toEqual("8") 
    })

    it("should tie against itself", function() {
      var board = ["-", "-", "-", 
                   "-", "-", "-",
                   "-", "-", "-"]

      board[4] = "x"; board[ai.smartMove("o", board)] = "o"
      board[ai.smartMove("x", board)] = "x"; board[ai.smartMove("o", board)] = "o"
      board[ai.smartMove("x", board)] = "x"; board[ai.smartMove("o", board)] = "o"
      board[ai.smartMove("x", board)] = "x"; board[ai.smartMove("o", board)] = "o"
      board[ai.smartMove("x", board)] = "x"

      var gameStatus = new GameStatus()
      expect(gameStatus.tie(board)).toEqual(true)
    })
  }) 
}) 
