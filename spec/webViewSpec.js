describe("The web view", function() {
  var view = new WebView()

  var element = document.createElement("i")
  element.id = "move0"; element.style.color = "#fff"; element.style.cursor = "pointer"
  document.body.appendChild(element)

  afterEach(function() {
    element.className = ""; element.style.color = ""; element.style.cursor = ""
  })

  describe("attachs a listener to move spaces", function() {
    it("attaches a listener to a move space", function() {
      spyOn(element, "addEventListener")
      view.attachListeners(function () {}, 1)
      expect(element.addEventListener).toHaveBeenCalled()
    })

    it("attaches a listener to each move space", function() {
      var element0 = document.createElement("i"); element1 = document.createElement("i")
      element0.id = "move1"; element1.id = "move2"
      document.body.appendChild(element0); document.body.appendChild(element1)
      spyOn(element0, "addEventListener"); spyOn(element1, "addEventListener")

      view.attachListeners(function () {}, 3)
      expect(element0.addEventListener).toHaveBeenCalled()
      expect(element1.addEventListener).toHaveBeenCalled()
    })
  })

  describe("detaches a listener to move spaces", function() {
    it("detaches a listener to a move space", function() {
      spyOn(element, "removeEventListener")
      view.removeListener(function () {}, element)
      expect(element.removeEventListener).toHaveBeenCalled()
    })

    it("changes cursor so user knows the space cannot be clicked", function() {
      view.removeListener(function () {}, element)
      expect(element.style.cursor).toEqual("default")
    })

    it("detaches a listener to each move space", function() {
      var element0 = document.createElement("i"); element1 = document.createElement("i")
      element0.id = "move1"; element1.id = "move2"
      document.body.appendChild(element0); document.body.appendChild(element1)
      spyOn(view, "removeListener")
      
      var listenFn = function () {}
      view.removeListeners(listenFn, 3)

      expect(view.removeListener.calls.count()).toEqual(3)
    })
  })

  describe("making a move", function() {
    it("changes color from white to black", function() {
      view.turn("x", 0)
      expect(element.style.color).toEqual("rgb(0, 0, 0)")
    })

    it("changes cursor to default", function() {
      view.turn("x", 0)
      expect(element.style.cursor).toEqual("default")
    })

    it("places an x mark", function() {
      view.turn("x", 0)
      expect(element.className).toEqual("fa fa-close fa-5x")
    })

    it("places an o mark", function() {
      view.turn("o", 0)
      expect(element.className).toEqual("fa fa-circle-o fa-5x")
    })

    it("removes onclick attribute", function() {
      spyOn(view, "removeListener")
      view.turn("x", 0)
      expect(view.removeListener).toHaveBeenCalled()
    })
  })

  describe("game status", function() {
    var statusElement = document.createElement("i")
    statusElement.id = "status"; statusElement.className = "fa fa-meh-o fa-3x"; statusElement.style.color = "rgb(255, 255, 255)"
    document.body.appendChild(statusElement)

    afterEach(function() {
      statusElement.className = ""; statusElement.style.color = "" 
    })

    it("displays meh for tie", function() {
      view.tieStatus()
      expect(statusElement.className).toEqual("fa fa-meh-o fa-3x")
      expect(statusElement.style.cursor).toEqual("default")
      expect(statusElement.style.color).toEqual("rgb(0, 0, 0)")
    })

    it("displays frown for loss", function() {
      view.loseStatus()
      expect(statusElement.className).toEqual("fa fa-frown-o fa-3x")
      expect(statusElement.style.cursor).toEqual("default")
      expect(statusElement.style.color).toEqual("rgb(0, 0, 0)")
    })
  })
})
