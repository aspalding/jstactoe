#Javascript Tic Tac Toe
###Requirements

* npm
* phantomjs

All of the following are available through brew and can be installed easily using:

	$ brew install npm phantomjs
    $ npm install -g grunt-cli
    
The game runs without any of the dependencies in recent versions of Chrome and Firefox. 

###Configuration
Install all the required dependancies.

	$ npm install

###Tests
The default argument to grunt is set to Jasmine.

	$ grunt
	
###Play the game!
Navigate to `index.html`

	$ open index.html
